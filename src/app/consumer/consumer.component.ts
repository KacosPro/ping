import { Component, OnInit } from '@angular/core';

import { environment } from '../../environments/environment';

export interface RequestMethod {
  value: string;
  viewValue: string;
}

export interface Options {
  requestMethod: string;
  body: string;
}

const methods = [
  'GET',
  'POST',
  'PATCH',
  'PUT',
  'DELETE',
]

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.css']
})
export class ConsumerComponent implements OnInit {

  requestMethod = '';
  requestData = '';
  loading = false;
  response = '';
  requestTime = '';

  ngOnInit() { }

  requestMethods: RequestMethod[] = methods.map(method => ({
    value: method,
    viewValue: method
  }));

  async onSubmit() {
    if (!this.loading && this.requestMethod !== '') {
      this.loading = true;
      const options = <any>{
        method: this.requestMethod,
      }
      if (options.method.toLowerCase() !== 'get') {
        options.body = this.requestData;
      }
      try {
        const requestStarted = Date.now();
        const response = await fetch(environment.apiUrl, options);
        const requestFinished = Date.now();
        this.response = await response.text();
        this.requestTime = `${(requestFinished - requestStarted)/1000} s`
        this.loading = false;
      } catch (error) {
        this.response = error.message;
        this.loading = false;
      }
    }
  }

}
